package main

import (
	"io"
	// "bytes"
	"log"
	"net/http"
)

func main() {

	http.Handle("/toto", http.FileServer(http.Dir("/truct")))
	http.HandleFunc("/github_callback", GithubCallbackHandler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

type user struct {
	Id      string `json:"id"`
	PRCount int
}

type pullRequestEvent struct {
	Number int    `json:"number"`
	Action string `json:"action"`
}

func GithubCallbackHandler(w http.ResponseWriter, req *http.Request) {
	req.ParseForm()
	log.Println(req.Form)
	// buf := new(bytes.Buffer)
	// buf.ReadFrom(req.Body)
	// log.Println(buf.String())
	io.WriteString(w, "")
}
